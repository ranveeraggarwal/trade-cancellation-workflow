app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider 
    .when('/search', { 
      controller: 'searchController', 
      templateUrl: 'views/search.html' 
    }) 
    .when('/report', { 
      templateUrl: 'views/report.html' 
    }) 
    .when('/about', { 
      templateUrl: 'views/about.html' 
    }) 
    .when('/signout', { 
      redirectTo: '/'
    }) 
    .when('/workflow/:tid', { 
      controller: 'workflowController',
      templateUrl: 'views/workflow.html'
    }) 
    .when('/', { 
      controller: 'homeController', 
      templateUrl: 'views/home.html' 
    }) 
    .otherwise({ 
      redirectTo: '/'
    }); 
  //$locationProvider.html5Mode(true);
}]);