searchTrades.directive("filterTabs", function() {
  return {
    restrict: "E",
    templateUrl: "directives/templates/searchTrades/filter-tabs.html"
  };
});