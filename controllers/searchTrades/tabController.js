searchTrades.controller('tabController', ['$scope', '$filter', function ($scope, filter) {
	this.tab = 1;

	$scope.currTab = 1;

	this.isSet = function(checkTab) {
		return this.tab === checkTab;
	};

	this.setTab = function(activeTab) {
		this.tab = activeTab;
		$scope.currTab = activeTab;
	};  

	/* For the date */
	$scope.today = function() {
	    $scope.dt = new Date();
	};
	
	$scope.today();

	$scope.updateTID = function(tradeID) {
		$scope.tid = tradeID;
	};

	$scope.updateBID = function(bulkID) {
		$scope.bid = bulkID;
	};

	$scope.updateGID = function(groupID) {
		$scope.gid = groupID;
	};	

	$scope.updateRows = function() {
		if($scope.currTab === 1) {
			$scope.rowCollection = [
				{firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'}
			];
		} else if ($scope.currTab === 2){
			$scope.rowCollection = [
				{firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
				{firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'}
			];
		} else if ($scope.currTab === 3){
			$scope.rowCollection = [
				{firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
				{firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
				{firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'}
			];
		} else {
			$scope.rowCollection = [
		        {firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
		        {firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'},
		        {firstName: 'Francoise', lastName: 'Frere', birthDate: new Date('1955-08-27'), balance: 42343, email: 'raymondef@gmail.com'}
    		];

		}
	}
	
    $scope.predicates = ['firstName', 'lastName', 'birthDate', 'balance', 'email'];
    $scope.selectedPredicate = $scope.predicates[0];
}]);